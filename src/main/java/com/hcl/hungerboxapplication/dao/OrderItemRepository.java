package com.hcl.hungerboxapplication.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.hungerboxapplication.model.Item;
import com.hcl.hungerboxapplication.model.Order;
import com.hcl.hungerboxapplication.model.OrderItems;

public interface OrderItemRepository  extends JpaRepository<OrderItems, Long>{

	List<Item> findByOrder(Order order);
}
