package com.hcl.hungerboxapplication.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.hungerboxapplication.model.Item;
import com.hcl.hungerboxapplication.model.Vendor;

public interface ItemRepository extends JpaRepository<Item, Long>{

	Item findByItemIdAndVendor(long itemId,Vendor vendor);
}
