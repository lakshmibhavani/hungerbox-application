package com.hcl.hungerboxapplication.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.hungerboxapplication.model.Order;

public interface OrderRepository extends JpaRepository<Order, Long>{

}
