package com.hcl.hungerboxapplication.service;

import java.util.List;
import java.util.stream.Collectors;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.hcl.hungerboxapplication.dao.EmployeeRepository;
import com.hcl.hungerboxapplication.dao.HungerBox;
import com.hcl.hungerboxapplication.dao.ItemRepository;
import com.hcl.hungerboxapplication.dao.OrderItemRepository;
import com.hcl.hungerboxapplication.dao.OrderRepository;
import com.hcl.hungerboxapplication.dao.VendorRepository;
import com.hcl.hungerboxapplication.model.Employee;
import com.hcl.hungerboxapplication.model.Item;
import com.hcl.hungerboxapplication.model.Order;
import com.hcl.hungerboxapplication.model.OrderItems;
import com.hcl.hungerboxapplication.model.Vendor;
import com.hcl.hungerboxapplication.dto.OrderDto;
import com.hcl.hungerboxapplication.exception.EmployeeNotFoundException;

@Service
public class OrderServiceImpl{
	
	private static final long  HUNGERBOXNUMBER = 9550608030L;

	@Autowired
	OrderItemRepository  orderItemRepository;
	
	@Autowired
	EmployeeRepository employeeRepository;
	
	@Autowired
	VendorRepository vendorRepository;

	@Autowired
	OrderRepository orderRepository;
	
	@Autowired
	ItemRepository itemRepository;
	
	@Autowired
	RestTemplate restTemplate;
	
	public Order makeOrder(OrderDto orderDto) throws JSONException {
		Employee employee=employeeRepository.findById(orderDto.getEmployeeId()).orElseThrow(()->new EmployeeNotFoundException("employee Not Found"));
		Vendor vendor=vendorRepository.findById(orderDto.getVendorId()).orElseThrow(()->new EmployeeNotFoundException("vendor not found"));
		
		Order order=new Order();
		List<Item> items=orderDto.getItems();
		
		List<Double> d=items.stream().map(x->x.getUnitPrice()).collect(Collectors.toList());
		double sum=d.stream().mapToDouble(Double::doubleValue).sum();
		order.setEmployee(employee);
		order.setOrderPrice(sum);
		order.setQuantity(items.size());
		orderRepository.save(order);
		for(Item item:items) {
			//if(items.contains(item1)) {
			Item item1=itemRepository.findByItemIdAndVendor(item.getItemId(), vendor);
			if(item1!=null) {
			
				OrderItems orderItem=new OrderItems();
			//orderItem.setId(orderItem.getId()+1);
			orderItem.setItem(item);
			orderItem.setOrder(order);
			orderItemRepository.save(orderItem);
			//}
	
				
			
			}
			else {
				throw new EmployeeNotFoundException("the item is not found related to particular vendor");
			}
		
			}
		
		try {
		String uri = "http://localhost:9093/transaction";
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		JSONObject request = new JSONObject();
		request.put("fromMobileNumber",(long)employee.getPhone());
		request.put("toMobileNumber",HUNGERBOXNUMBER );
		request.put("transferAmmount", sum);
		HttpEntity<String> entity = new HttpEntity<String>(request.toString(), headers);

		ResponseEntity<String> response = restTemplate.postForEntity(uri, entity, String.class);

		System.out.println(response);
		}
		catch(Exception e) {
			throw new EmployeeNotFoundException("transaction is failure");
		}
		
		
		return order;
	}

	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}
}
