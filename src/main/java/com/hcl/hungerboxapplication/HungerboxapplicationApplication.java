package com.hcl.hungerboxapplication;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HungerboxapplicationApplication {

	public static void main(String[] args) {
		SpringApplication.run(HungerboxapplicationApplication.class, args);
	}

}
