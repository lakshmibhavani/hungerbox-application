package com.hcl.hungerboxapplication.controller;

import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.hungerboxapplication.dto.OrderDto;
import com.hcl.hungerboxapplication.model.Order;
import com.hcl.hungerboxapplication.service.OrderServiceImpl;

@RestController
@RequestMapping("/order")
public class OrderController {
	
	@Autowired
	OrderServiceImpl ordersService;
	
	@PostMapping("")
	public Order makingOrder(@RequestBody OrderDto orderDto) throws JSONException {
		return ordersService.makeOrder(orderDto);
	}

}
